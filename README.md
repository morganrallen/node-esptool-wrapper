# esptool-wrapper
Super (duper) minimal wrapper around `esptool.py` from [Espressif](https://github.com/espressif/esptool)


## usage

```
const esptool = require("esptool-wrapper");

esptool({
  chip: "esp32",
  port: "/dev/ttyS0m3th1",
  baud: 460800,
  files: {
    0x1000: "build/boot.bin",
    0x80000: "build/app.bin"
  }
}, function(err) {
  if(err) throw err; // hopefully I tell you *WHAT* went wrong

  console.log("Nothing went wrong!");
});
```

## config

### `chip`
Chip to flash. `auto`, `esp32` or `esp8266`. Will `auto` select if not provided (slightly slower)

### `port`
Serial port for device to be flashed

### `baud`
Speed to flash device at.

### `files`
Object of files to flash, with keys representing destination address
```
...
files: {
  0x10000: "build/app.bin"
}
```

Doesn't require hex notaion (but that looks cooler)

### `args`
Additional command line args. This is mostly for controlling reset behavior.

### `cmdArgs`
Additional `write_flash` arguments. Used to pass arguments *AFTER* the `write_flash` command.

### `exec`
path to `esptool.py`. Useful if not available in `$PATH` and testing.
